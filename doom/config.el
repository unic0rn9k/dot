;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

(setq fancy-splash-image "~/Pictures/sticker.png")


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Aksel Mannstaedt"
      user-mail-address "unic0rn9k@protonmail.com")

(setq explicit-shell-file-name (executable-find "fish"))

;;fonts
(setq doom-font (font-spec :family "Liga SFMono Nerd Font" :size 13)
      doom-big-font (font-spec :family "Liga SFMono Nerd Font" :size 20)
      doom-variable-pitch-font (font-spec :family "SF Pro" :size 15)
      doom-unicode-font (font-spec :family "Liga SFMono Nerd Font")
      doom-serif-font (font-spec :family "Liga SFMono Nerd Font" :weight 'Regular))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;;(setq doom-theme 'doom-gruvbox)
;;(setq doom-theme 'doom-dark+)
(setq doom-theme 'doom-vibrant)
;;(setq doom-theme 'doom-snazzy)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
(setq make-backup-files t)
(setq confirm-kill-emacs nil)

;;modeline (icons, config, battery)
;;(display-time-mode 1)                              ;Enable time in the mode-line
(display-battery-mode 1)                           ;display the battery
(setq doom-modeline-major-mode-icon t)             ;Show major mode name
(setq doom-modeline-enable-word-count t)           ;Show word count
(setq doom-modeline-modal-icon t)                  ;Show vim mode icon

(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
(add-hook! '+doom-dashboard-mode-hook (hide-mode-line-mode 1) (hl-line-mode -1))
(setq-hook! '+doom-dashboard-mode-hook evil-normal-state-cursor (list nil))

;;mixed pitch modes
;;;(defvar mixed-pitch-modes '(org-mode LaTeX-mode markdown-mode gfm-mode Info-mode)
;;;  "Modes that `mixed-pitch-mode' should be enabled in, but only after UI initialisation.")
;;;(defun init-mixed-pitch-h ()
;;;  "Hook `mixed-pitch-mode' into each mode in `mixed-pitch-modes'.
;;;Also immediately enables `mixed-pitch-modes' if currently in one of the modes."
;;;  (when (memq major-mode mixed-pitch-modes)
;;;    (mixed-pitch-mode 1))
;;;  (dolist (hook mixed-pitch-modes)
;;;    (add-hook (intern (concat (symbol-name hook) "-hook")) #'mixed-pitch-mode)))
;;;(add-hook 'doom-init-ui-hook #'init-mixed-pitch-h)
;;;(add-hook! 'org-mode-hook #'+org-pretty-mode) ;enter mixed pitch mode in org mode
;;;
;;;;;set mixed pitch font
;;; (after! mixed-pitch
;;;  (defface variable-pitch-serif
;;;    '((t (:family "serif")))
;;;    "A variable-pitch face with serifs."
;;;    :group 'basic-faces)
;;;  (setq mixed-pitch-set-height t)
;;;  (setq variable-pitch-serif-font (font-spec :family "SF Pro" :size 16 :weight 'Medium))
;;;  (set-face-attribute 'variable-pitch-serif nil :font variable-pitch-serif-font)
;;;  (defun mixed-pitch-serif-mode (&optional arg)
;;;    "Change the default face of the current buffer to a serifed variable pitch, while keeping some faces fixed pitch."
;;;    (interactive)
;;;    (let ((mixed-pitch-face 'variable-pitch-serif))
;;;      (mixed-pitch-mode (or arg 'toggle)))))
;;;
;;;(defvar required-fonts '("SF Pro" "Liga SFMono Nerd Font" ))
;;;(defvar available-fonts
;;;  (delete-dups (or (font-family-list)
;;;                   (split-string (shell-command-to-string "fc-list : family")
;;;                                 "[,\n]"))))
;;;(defvar missing-fonts
;;;  (delq nil (mapcar
;;;             (lambda (font)
;;;               (unless (delq nil (mapcar (lambda (f)
;;;                           (string-match-p (format "^%s$" font) f))
;;;                                         available-fonts))
;;;                                         font))
;;;                                         required-fonts)))
;;;(if missing-fonts
;;;    (pp-to-string
;;;     `(unless noninteractive
;;;        (add-hook! 'doom-init-ui-hook
;;;          (run-at-time nil nil
;;;                       (lambda ()
;;;                         (message "%s missing the following fonts: %s"
;;;                                  (propertize "Warning!" 'face '(bold warning))
;;;                                  (mapconcat (lambda (font)
;;;                                               (propertize font 'face 'font-lock-variable-name-face))
;;;                                             ',missing-fonts
;;;                                             ", "))
;;;                         (sleep-for 0.5))))))
;;;  ";; No missing fonts detected")

(after! org-roam
    (setq org-roam-capture-templates
          `(("s" "standard" plain "%?"
     :if-new
     (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
      "#+title: ${title}\n#+filetags: \n\n ")
     :unnarrowed t)
        ("d" "definition" plain
         "%?"
         :if-new
         (file+head "${slug}.org" "#+title: ${title}\n#+filetags: definition \n\n* Definition\n\n\n* Examples\n")
         :unnarrowed t)
        ("r" "ref" plain "%?"
           :if-new
           (file+head "${citekey}.org"
           "#+title: ${slug}: ${title}\n
\n#+filetags: reference ${keywords} \n
\n* ${title}\n\n
\n* Summary
\n\n\n* Rough note space\n")
           :unnarrowed t)
          ("p" "person" plain "%?"
           :if-new
           (file+head "${slug}.org" "%^{relation|some guy|family|friend|colleague}p %^{birthday}p %^{address}p
#+title:${slug}\n#+filetags: :person: \n"
                      :unnarrowed t)))))

(defvar emojify-disabled-emojis
  '(;; Org
    "◼" "☑" "☸" "⚙" "⏩" "⏪" "⬆" "⬇" "❓" "♥" "🕮"
    ;; Terminal powerline
    "✔"
    ;; Box drawing
    "▶" "◀")
  "Characters that should never be affected by `emojify-mode'.")

(defadvice! emojify-delete-from-data ()
  "Ensure `emojify-disabled-emojis' don't appear in `emojify-emojis'."
  :after #'emojify-set-emoji-data
  (dolist (emoji emojify-disabled-emojis)
    (remhash emoji emojify-emojis)))

(add-hook! '(mu4e-compose-mode org-msg-edit-mode) (emoticon-to-emoji 1))

(add-hook 'org-mode-hook #'+org-pretty-mode)
(setq org-pretty-entities-include-sub-superscripts nil) ;;doesn't play well with latex
(setq org-fontify-quote-and-verse-blocks t)

(use-package! org-appear
  :hook (org-mode . org-appear-mode)
  :config
  (setq org-appear-autoemphasis t
        org-appear-autosubmarkers t
        org-appear-autolinks nil)
  (run-at-time nil nil #'org-appear--set-elements))

(defun locally-defer-font-lock ()
  "Set jit-lock defer and stealth, when buffer is over a certain size."
  (when (> (buffer-size) 50000)
    (setq-local jit-lock-defer-time 0.05
                jit-lock-stealth-time 1)))

(add-hook 'org-mode-hook #'locally-defer-font-lock)

;;make bullets look better
(after! org-superstar
  (setq org-superstar-headline-bullets-list '("♥" "♡" "✿" "✤" "✜" "◆" "✸" "▶")
        org-superstar-prettify-item-bullets t ))

(setq org-ellipsis " ▾ "
      org-hide-leading-stars t
      org-priority-highest ?A
      org-priority-lowest ?E
      org-priority-faces
      '((?A . 'all-the-icons-red)
        (?B . 'all-the-icons-orange)
        (?C . 'all-the-icons-yellow)
        (?D . 'all-the-icons-green)
        (?E . 'all-the-icons-blue)))

;;lets replace some unicode chars
(appendq! +ligatures-extra-symbols
          `(:checkbox      ""
            :pending       ""
            :checkedbox    "☑"
            :list_property ""
            :em_dash       "—"
            :ellipses      "…"
            :arrow_right   "→"
            :arrow_left    "←"
            :property      ""
            :options       ""
            :startup       "⏻"
            :html_head     "🅷"
            :html          "🅗"
            :latex_class   "🕮"
            :latex_header  "🕮"
            :beamer_header "🅑"
            :latex         "🅛"
            :attr_latex    "🄛"
            :attr_html     "🄗"
            :attr_org      "⒪"
            :begin_quote   "❝"
            :end_quote     "❞"
            :caption       "☰"
            :header        "›"
            :begin_export  "⏩"
            :end_export    "⏪"
            :properties    "⚙"
            :end           "←"
            :title         "♥"
            :match_arrow   "⇒"
))
(set-ligatures! 'org-mode
  :merge t
  :checkbox      "[ ]"
  :pending       "[-]"
  :checkedbox    "[X]"
  :list_property "::"
  :em_dash       "---"
  :ellipsis      "..."
  :arrow_right   "->"
  :arrow_left    "<-"
  :match_arrow   "=>"
  :title         "#+title:"
  :subtitle      "#+subtitle:"
  :author        "#+author:"
  :date          "#+date:"
  :property      "#+property:"
  :options       "#+options:"
  :startup       "#+startup:"
  :macro         "#+macro:"
  :html_head     "#+html_head:"
  :html          "#+html:"
  :latex_class   "#+latex_class:"
  :latex_header  "#+latex_header:"
  :beamer_header "#+beamer_header:"
  :latex         "#+latex:"
  :attr_latex    "#+attr_latex:"
  :attr_html     "#+attr_html:"
  :attr_org      "#+attr_org:"
  :begin_quote   "#+begin_quote"
  :end_quote     "#+end_quote"
  :caption       "#+caption:"
  :header        "#+header:"
  :begin_export  "#+begin_export"
  :end_export    "#+end_export"
  :results       "#+RESULTS:"
  :title         "#+TITLE:"
  :property      ":PROPERTIES:"
  :end           ":END:"
)
(plist-put +ligatures-extra-symbols :name "⁍")


;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
;; (setq doom-font (font-spec :family "FiraCode Nerd Font" :size 15) ;; --> == != >=
;;       doom-variable-pitch-font (font-spec :family "Alegreya" :size 18))

;;(setq doom-font (font-spec :family "SF Mono" :size 15)
;;      doom-big-font (font-spec :family "SF Mono" :size 21)
;;      doom-variable-pitch-font (font-spec :family "Roboto Mono" :size 15)
;;      doom-unicode-font (font-spec :family "SF Mono")
;;      doom-serif-font (font-spec :family "Roboto Mono" :weight 'light))

(defun my-emoji-fonts ()
  (set-fontset-font t 'symbol "Mutant") ;; 💔
  (set-fontset-font t 'symbol "Twemoji" nil 'append))

(if (daemonp)
    (add-hook 'server-after-make-frame-hook #'my-emoji-fonts)
  (my-emoji-fonts))

;; (add-hook! 'org-mode-hook #'mixed-pitch-mode)
;; (setq mixed-pitch-variable-pitch-cursor nil)

(use-package! ligature
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

(custom-set-variables
 '(org-directory "~/notes_n_stuff")
 '(org-agenda-files (mapcar 'abbreviate-file-name
(split-string
(shell-command-to-string "fd '.org$' ~/notes_n_stuff")
"\n"))))

(setq org-enable-notifications t) ;; This doesn't work
(setq alert-default-style 'notifications)
;;(setq inhibit-compacting-font-caches t)            ;Don't compact font caches in gc

;; (after! org
;;   (setq org-special-ctrl-a/e t)
;;   (setq org-special-ctrl-k t))

;; (add-hook! org-mode (electric-indent-local-mode -1))

;; (add-hook! org-mode :append
;;            #'visual-line-mode
;; )

;; Show images when opening a file.
(setq org-startup-with-inline-images t)
(setq org-startup-with-inline-latex t)

;; Show images after evaluating code blocks.
;;(add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
(setq org-roam-directory (expand-file-name "roam/" org-directory))
(setq reftex-default-bibliography (expand-file-name "big.bib" org-directory))

(after! org (setq org-hide-emphasis-markers t))
(add-hook! org-mode :append #'org-appear-mode)

;;auto toggle between preview/raw latex
(use-package! org-fragtog
  :hook (org-mode . org-fragtog-mode))

(use-package! org-pretty-table
  :commands (org-pretty-table-mode global-org-pretty-table-mode))

(setq my-org-latex-preview-scale 0.5)   ; depends on the font used in emacs or just on user preference
(defun org-latex-preview-advice (orig-func &rest args)
  (let ((old-val (copy-tree org-format-latex-options)))     ; plist-put is maybe-destructive, weird. So, we have to restore old value ourselves
    (setq org-format-latex-options (plist-put org-format-latex-options
                                              :scale
                                              (* my-org-latex-preview-scale (expt text-scale-mode-step text-scale-mode-amount))))
    (apply orig-func args)
    (setq org-format-latex-options old-val)))
(advice-add 'org-latex-preview :around #'org-latex-preview-advice)

(load "da-kalender") ;; Danish calender for holidays 'n stuff

(require 'guess-language)

;; Optionally:
(setq guess-language-languages '(en da))
(setq guess-language-langcodes
  '((en . ("en_US-large" "English"))
    (da . ("da_DK" "Danish"))))
(add-hook 'text-mode-hook (lambda () (guess-language-mode 1)))

(use-package! org-fancy-priorities
; :ensure t
  :hook
  (org-mode . org-fancy-priorities-mode)
  :config
   (setq org-fancy-priorities-list '("FUCK->" "✨" "❈" "!" ".")))

(use-package! org-super-agenda
  :after org-agenda
  :init

  (setq org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-include-deadlines t
      org-agenda-block-separator nil
      org-agenda-compact-blocks t
      org-agenda-start-day nil ;; i.e. today
      org-agenda-span 10
      org-agenda-start-on-weekday nil)

  (setq org-agenda-custom-commands
        '(("c" "Super view"
           ((agenda ""  ((org-agenda-overriding-header "")
                        (org-super-agenda-groups
                         '((:name ""
                                :time-grid t
                                :order 1
                                :scheduled today
                                :date today
                                :deadline today
                                :and (:priority>= "C" :not (:scheduled past :deadline past))
                                )
                           (:discard (:anything t));:priority< "B"))
                           ))))
            (alltodo "" ((org-agenda-overriding-header "")
                         (org-super-agenda-groups
                          '((:log t)
                            (:name "Member"
                                   :tag "member"
                                   :order 10000)
                            (:name "Overdue"
                                   :deadline past
                                   :scheduled past
                                   :order 3)
                            (:name "Important"
                                   :priority "A"
                                   :order 2)
                            (:name "Due Today"
                                   :deadline today
                                   :order 1)
                            (:name "Scheduled Soon"
                                   :scheduled future
                                   :order 8)
                            (:name "Meetings"
                                   :and (:todo "MEET" :scheduled future)
                                   :order 10)
                            (:name "DEBT"
                                   :tag "debt"
                                   :order 9)
                            (:discard (:anything t))))))))))
  :config
  (org-super-agenda-mode))

;;(use-package! ein ;; Found out doom has a buildin EIN module.
;;  :config
;;  (setq ob-ein-languages
;;   (quote
;;    (("ein-python" . python)
;;     ("ein-R" . R)
;;     ("ein-r" . R)
;;     ("ein-rust" . rust)
;;     ("ein-haskell" . haskell)
;;     ("ein-julia" . julia))))
;;  )

;;(after! ein:ipynb-mode                  ;
;;  (poly-ein-mode 1)
;;  (hungry-delete-mode -1)
;;  )

(use-package! good-scroll
  :config
  (good-scroll-mode 1)
)

;; Make TODO's and sub-TODO's have correct identing
;;(after! org-agenda
;;  (setq org-agenda-prefix-format
;;        '((agenda . " %i %-12:c%?-12t% s")
;;          ;; Indent todo items by level to show nesting
;;          (todo . " %i %-12:c%l")
;;          (tags . " %i %-12:c")
;;          (search . " %i %-12:c")))
;;  (setq org-agenda-include-diary t))

;; (use-package! org-aler
;;   :config
;;   org-alert-enable
;; )

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Enable loopback so that pinentry will pop up in emacs
(pinentry-start) ;; Pinentry's gui broke. Now emacs will be my pinentry!
(shell-command "gpgconf --reload gpg-agent")
;; Start GPG agent with SSH support
;;(shell-command "gpg-connect-agent /bye")

(setq sage-shell:sage-root "/home/unic0rn9k/Downloads/sage-9.3-Ubuntu_20.04-x86_64/SageMath")
;; Ob-sagemath supports only evaluating with a session.
(setq org-babel-default-header-args:sage '((:session . t)
                                           (:results . "file")
                                           (:exports . "results")
                                           ))

;; C-c c for asynchronous evaluating (only for SageMath code blocks).
(with-eval-after-load "org"
  (define-key org-mode-map (kbd "C-c c") 'ob-sagemath-execute-async))

;; Do not confirm before evaluation
;;(setq org-confirm-babel-evaluate nil)

(setq +ivy-project-search-engines '(rg))

(use-package! all-the-icons-ivy
  :init (add-hook 'after-init-hook 'all-the-icons-ivy-setup))

(use-package! pdf-view
  :hook (pdf-tools-enabled . pdf-view-midnight-minor-mode)
  :hook (pdf-tools-enabled . hide-mode-line-mode)
  :config
  (setq pdf-view-midnight-colors '(
        "#dcdfe4" . "#282c34" )))

(setq +zen-text-scale 0.8)

(setq +latex-viewers '(zathura pdf-tools))
(setq org-latex-pdf-process '("latexmk -f -pdf -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"))

(setq ispell-dictionary "dansk")

(use-package! fish-mode
  :mode "\\.fish\\'")

;;(add-to-list 'auto-mode-alist '("\\.d\\'" . d-mode))
;; (use-package! d-mode
;;   :mode "\\.d\\'")
;;
;; ;;(use-package! flycheck-dmd-dub
;; ;;  :mode "\\.d\\'")
;; ;;(add-hook 'd-mode-hook 'flycheck-dmd-dub-set-variables)
;;
;; ;;(use-package! popwin)
;; (use-package! auto-complete
;;   :mode "\\.d\\'")
;; (use-package! ac-dcd-setup
;;   :mode "\\.d\\'")
;; (add-hook 'd-mode-hook 'auto-complete-mode)

;;(use-package! pest-mode
;;  :mode "\\.pest\\'"
;;  :hook (pest-mode . flymake-mode))
;;(autoload 'pest-mode "pest-mode")
;;(add-to-list #'auto-mode-alist '("\\.pest\\'" . pest-mode))
;;
;;(use-package! flymake-pest)
;;(use-package! company-tabnine :after company :config cl-pushnew 'company-tabnine (default-value 'company-backends))
;;(add-to-list 'company-backends #'company-tabnine)
;;
;;
;;
;; (when (getenv "WAYLAND_DISPLAY")
;;  (setq wl-copy-p nil
;;
;;        interprogram-cut-function
;;        (lambda (text)
;;          (setq-local process-connection-type 'pipe)
;;                (setq wl-copy-p (start-process "wl-copy" nil "wl-copy" "-f" "-n"))
;;          (process-send-string wl-copy-p text)
;;          (process-send-eof wl-copy-p))
;;
;;        interprogram-paste-function
;;        (lambda ()
;;          (unless (and wl-copy-p (process-live-p wl-copy-p))
;;            (shell-command-to-string "wl-paste -n | tr -d '\r'")))))
;;
;;(when (getenv "WAYLAND_DISPLAY")
;;  (setq wl-copy-p nil
;;
;;        interprogram-cut-function
;;        (lambda (text)
;;          (setq-local process-connection-type 'pipe)
;;          (setq wl-copy-p (start-process "wl-copy" nil "wl-copy" "--foreground" "--trim-newline"))
;;          (process-send-string wl-copy-p text)
;;          (process-send-eof wl-copy-p))
;;
;;        interprogram-paste-function
;;        (lambda ()
;;          (unless (and wl-copy-p (process-live-p wl-copy-p))
;;            (shell-command-to-string "wl-paste -n | tr -d '\r'")))))
;;
;;(when (getenv "WAYLAND_DISPLAY")
;;  (setq wl-copy-p nil
;;        interprogram-cut-function (lambda (text)
;;                                    (setq-local process-connection-type 'pipe)
;;                                    (setq wl-copy-p (start-process "wl-copy" nil "wl-copy" "-f" "-n"))
;;                                    (process-send-string wl-copy-p text)
;;                                    (process-send-eof wl-copy-p))
;;        interprogram-paste-function (lambda ()
;;                                      (unless (and wl-copy-p (process-live-p wl-copy-p))
;;                                        (shell-command-to-string "wl-paste -n | tr -d '\r'")))))

(when (getenv "WAYLAND_DISPLAY")
  (setq interprogram-paste-function
        (lambda ()
                (shell-command-to-string "wl-paste -n | tr -d '\r'"))))


;;(defun update-org-latex-fragment-scale ()
;;  (let ((text-scale-factor (expt text-scale-mode-step text-scale-mode-amount)))
;;    (plist-put org-format-latex-options :scale (* 4 text-scale-factor)))
;;)
;;(add-hook 'text-scale-mode-hook 'update-org-latex-fragment-scale)

;;(use-package! zig-mode
;;  :hook ((zig-mode . lsp-deferred))
;;  :custom (zig-format-on-save nil)
;;  :config
;;  (after! lsp-mode
;;    (add-to-list 'lsp-language-id-configuration '(zig-mode . "zig"))
;;    (lsp-register-client
;;      (make-lsp-client
;;        :new-connection (lsp-stdio-connection "/usr/bin/zls")
;;        :major-modes '(zig-mode)
;;        :server-id 'zls))))


;;(add-to-list 'org-export-latex-classes
;;        '(""
;;             "\\usepackage[hidelinks]{hyperref}
;;             \\usepackage[ddmmyyyy]{datetime}
;;             \\setlength{\\parindent}{Opt}"
;;       ))

;;(treemacs-get-icon-value (lsp-treemacs-symbol-kind->icon 6))
