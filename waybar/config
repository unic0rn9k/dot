{
    // "layer": "top", // Waybar at top layer
    // "position": "bottom", // Waybar position (top|bottom|left|right)
    "height": 10, // Waybar height (to be removed for auto height)
    // "width": 1200, // Waybar width
    "margin-top": 4,
    "margin-right": 8,
    "margin-left": 8,
    "margin-bottom": 6,
    // Choose the order of the modules
    "modules-left": ["custom/menu", "network", "battery", "custom/power"],
    "modules-right": ["tray", "pulseaudio", "backlight", "memory", "cpu", "clock", "custom/swaync"],
    "modules-center": ["sway/workspaces", "custom/music"],
    // Modules configuration

    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },

    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },

    "sway/workspaces": {
        "disable-scroll": true,
        "format": "{name}"
    },

    "custom/nm": {
        "exec": "timeout 5s ~/.config/sway/nm-tray.sh",
        "escape": true,
    },
    "sway/window": {
      "format": "{}",
      "max-length": 50
    },
    "custom/waybar-media": {
        "return-type": "json",
        "exec": "~/.config/waybar/waybar-media.py status",
        "on-click": "~/.config/waybar/waybar-media.py playpause",
        "on-scroll-up": "~/.config/waybar/waybar-media.py previous",
        "on-scroll-down": "~/.config/waybar/waybar-media.py next",
        "escape": true
    },
    "idle_inhibitor": {
         "format": "{icon}",
         "format-icons": {
             "activated"   : "",
             "deactivated" : "",
         }
    },
    "clock": {
        // "timezone": "America/New_York",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format-alt": "{:%Y-%m-%d}"
    },
    "cpu": {
        "format": "{usage}%  ",
        "tooltip": false
    },
    "memory": {
        "format": "{}% "
    },
    "temperature": {
        "thermal-zone": 2,
        "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
        "critical-threshold": 80,
        "format-critical": "{temperatureC}°C {icon}",
        "format": "{temperatureC}°C {icon}",
        "interval": 60,
        "format-icons": ["", "", ""]
    },
    "backlight": {
        "interval": 5,
        "format": "{percent}% ",
        "format-alt-click": "click-right",
        "on-scroll-down": "brightnessctl -c backlight set 5%-",
        "on-scroll-up": "brightnessctl -c backlight set +5%"
    },
    "battery": {
        "states": {
          // "good": 95,
          "warning": 30,
          "critical": 15
        },
        "format-icons": [" ", " ", " ", " ", " "],
        "format": "{icon}  {capacity}%",
        "format-charging": "  {capacity}%",
        "format-plugged" : "  {capacity}%",
        "format-alt": "{time}  {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
    },
    "network": {
        "interface": "wlan0", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ETH",
        "format-linked": "{ifname} (No IP) LINK",
        "format-disconnected": "Disconnected",
        "interval": 60,
        "on-click": "exec gnome-control-center wifi"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} {format_source}",
        "format-bluetooth-muted": "{icon} {format_source}",
        "format-muted": "{format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphone": " ",
            "hands-free": " ",
            "headset": " ",
            "phone": " ",
            "portable": " ",
            "car": " ",
            "default": ["  ", "  ", "  "]
        },
        "on-click": "gnome-control-center sound"
    },
    "custom/weather": {
        "exec": "curl 'https://wttr.in/?format=1'",
        "interval": 3600
    },
    "custom/pacman": {
        "format": "C {} N D Y",
        "interval": 3600,                     // every hour
        // "restart-interval": 3600,
        "exec": "pacman -Sup | wc -l",       // # of updates
        "on-click": "kitty update.sh; pkill -SIGRTMIN+8 waybar", // update system
        "signal": 8,
    },
    "custom/swaync": {
        "format": "{} ",
        "interval": 2,                     // every hour
        // "restart-interval": 3600,
        "exec": "swaync-client -c",       // # of updates
        "on-click": "swaync-client -t -sw", // update system
        "signal": 8,
    },

    "custom/menu": {
        "format": "🔥",
        "on-click": "~/.config/eww/launch_eww_side",
    },
    "disk": {
        "interval": 30,
        "format": "{percentage_free}% left in HDD",
        "path": "/run/media/bryan/6331d39c-2e68-4798-a70e-0aa11b783705/",
    },
    "custom/power": {
        "format": "",
        "on-click": "nwgbar",
        "escape": true,
        "tooltip": false 
    },

    "custom/music": {
        "format": "{}",
        "max-length": 20,
        "interval": 5,
        "exec": "/home/unic0rn9k/.config/waybar/mediaplayer.sh 2> /dev/null",
        "tooltip": false,
        "on-click": "playerctl play-pause",
        "on-scroll-up": "playerctl previous",
        "on-scroll-down": "playerctl next"
    },

    "wlr/taskbar": {
        "format": "{icon}",
        "icon-size": 15,
        "tooltip-format": "{title}",
        "on-click": "activate",
        "on-click-right": "minimize",
        "on-click-middle": "close"
    }
}
